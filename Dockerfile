# Perform the image build in two stages so that no unncessary files are present
# in the final image.

# Download the Litecoin binaries and verify checksum.
FROM debian:bullseye AS build

ARG LITECOIN_VERSION=0.18.1

WORKDIR /tmp
RUN apt-get update
RUN apt-get --yes install wget gpg
# Download binaries
RUN wget \
    "https://download.litecoin.org/litecoin-${LITECOIN_VERSION}/linux/litecoin-${LITECOIN_VERSION}-x86_64-linux-gnu.tar.gz"
# Download checksums
RUN wget \
    "https://download.litecoin.org/litecoin-${LITECOIN_VERSION}/linux/litecoin-${LITECOIN_VERSION}-linux-signatures.asc"
# Download Litecoin GPG public key
RUN gpg --keyserver keyserver.ubuntu.com --recv-key 0xFE3348877809386C
# Verify checksums
RUN gpg --verify litecoin-${LITECOIN_VERSION}-linux-signatures.asc
RUN sha256sum \
    --check litecoin-${LITECOIN_VERSION}-linux-signatures.asc \
    | grep OK
# Extract
RUN tar xvf litecoin-${LITECOIN_VERSION}-x86_64-linux-gnu.tar.gz \
    --transform s/litecoin-${LITECOIN_VERSION}/litecoin/

# Output image
FROM debian:bullseye-slim
COPY --from=build \
    /tmp/litecoin/bin/litecoind \
    /usr/local/bin
RUN adduser --system litecoin
WORKDIR /home/litecoin
USER litecoin
EXPOSE 9332-9333

ENTRYPOINT /usr/local/bin/litecoind
