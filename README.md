# DevOps Test

## Docker

### Build Image
```shell
docker build --tag litecoin .
```

### Run Image
```shell
docker run --rm litecoin:latest
```

## GitLab CI/CD
See [`.gitlab-ci.yml`](.gitlab-ci.yml).

## Kubernetes
The `StatefulSet` configuration can be found in [`k8s/litecoin.yml`](k8s/litecoin.yml).   
Tested locally using `minikube`.

```shell
minikube start
# Push the locally built image to the minikube image cache.
minikube cache add litecoin:latest
kubectl apply -f ./k8s/litecoin.yml
kubectl get pods
```

## Terraform
See [`terraform/main.tf`](terraform/main.tf).

### Apply
1. ```shell
    terraform init
    ```
2. ```shell
    export AWS_DEFAULT_REGION=<aws-region>
    ```
3. ```shell
    terraform apply
    ```
### Test
1. Add access keys to the created user and configure a new profile for the `aws` CLI to use them.
2. Assume the role using the ARN of the _Assumable Role_.
3. Example:   
   ```shell
   aws --profile test \
      sts assume-role \
      --role-arn arn:aws:iam::****:role/k****n/K****nAssumableRole \
      --role-session-name test \
      --output yaml
   ```   
   Output:   
   ```yaml
   AssumedRoleUser:
   Arn: arn:aws:sts::****:assumed-role/K****nAssumableRole/test
   AssumedRoleId: ****:test
   Credentials:
   AccessKeyId: ****
   Expiration: '2021-12-12T16:47:15+00:00'
   SecretAccessKey: ****
   SessionToken: ****
   ```

## Script Kiddies
**Skipped**   

I had some time during the weekend to work on the test, but that time is running out.

Here is a script that I wrote some time back that does use `sed`, `grep`, `awk`:   
https://gist.github.com/daharon/42d71fd9abf5f76e7cea

## Script Grown-ups
**Skipped**

This is an example of a partial compiler front-end using 
Rust's [LALRPOP](https://crates.io/crates/lalrpop) library that
I was messing with a while back.   
It's not string _manipulation_, per se, but sometimes a parser-generator is better than simple regular expressions.   

Grammar:  https://gitlab.com/daharon/lalrpop-test/-/blob/master/test-1/src/grammar.lalrpop   
Tests:  https://gitlab.com/daharon/lalrpop-test/-/blob/master/test-1/src/main.rs#L103-851   

