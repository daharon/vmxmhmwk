terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 3.69.0"
    }
  }
}

provider "aws" {
  default_tags {
    tags = {
      Environment = "kraken-devops-test"
    }
  }
}

# For access to the current account properties.
data "aws_caller_identity" "current" {}

resource "aws_iam_role" "kraken_assumable_role" {
  name = "KrakenAssumableRole"
  path = "/kraken/"
  assume_role_policy = jsonencode({
    Version = "2012-10-17"
    Statement = [
      {
        Action = "sts:AssumeRole"
        Effect = "Allow"
        Sid    = ""
        Principal = {
          AWS = "arn:aws:iam::${data.aws_caller_identity.current.account_id}:root"
        }
      },
    ]
  })
}

resource "aws_iam_policy" "kraken_assumable_role_policy" {
  name        = "KrakenAssumableRolePolicy"
  path        = "/kraken/"
  description = "Allow assumption of KrakenAssumableRole"

  policy = jsonencode({
    Version = "2012-10-17"
    Statement = [
      {
        Action = [
          "sts:AssumeRole",
        ]
        Effect   = "Allow"
        Resource = aws_iam_role.kraken_assumable_role.arn
      },
    ]
  })
}

resource "aws_iam_group" "kraken_assumable_role_group" {
  name = "KrakenAssumableRoleGroup"
  path = "/kraken/"
}

resource "aws_iam_group_policy_attachment" "kraken_assumable_role_group_policy_attachment" {
  group      = aws_iam_group.kraken_assumable_role_group.name
  policy_arn = aws_iam_policy.kraken_assumable_role_policy.arn
}

resource "aws_iam_user" "kraken_devops_test_user" {
  name = "DevOpsUser"
  path = "/kraken/"
}

resource "aws_iam_user_group_membership" "kraken_devops_test_user_groups" {
  user = aws_iam_user.kraken_devops_test_user.name
  groups = [
    aws_iam_group.kraken_assumable_role_group.name,
  ]
}
